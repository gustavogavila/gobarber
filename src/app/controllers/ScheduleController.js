import { endOfDay, parseISO, startOfDay } from 'date-fns';
import { Op } from 'sequelize';
import Appointment from '../models/Appointment';
import User from '../models/User';

class ScheduleController {
  async index(req, res) {
    const providerUser = await User.findOne({
      where: {
        id: req.userId,
        provider: true,
      },
    });

    if (!providerUser) {
      return res.json({ error: 'User is not a provider.' });
    }

    const { date } = req.query;

    const currentDay = parseISO(date);

    const appointments = await Appointment.findAll({
      where: {
        provider_id: req.userId,
        canceled_at: null,
        date: { [Op.between]: [startOfDay(currentDay), endOfDay(currentDay)] },
      },
      order: ['date'],
    });

    return res.json(appointments);
  }
}

export default new ScheduleController();
